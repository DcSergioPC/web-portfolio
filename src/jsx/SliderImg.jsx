import {motion, useMotionValue, useTransform} from "framer-motion";
import {useEffect, useRef, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

const variants = {
    enter: ({direction, rotateY}) => {
        return {
            rotateY: direction > 0 ? rotateY + 90 : rotateY - 90
        }
    },
    center: ({rotateY}) => {
        return {
            rotateY: 0,
            transition: {
                duration: 0.5 * ((90 - Math.abs(rotateY)) / 45),
                ease: "easeInOut",
            }
        }
    }
};
export default function SliderImg({setCertificate, mouseEvent, imgs}) {
    let [[page, direction], setPage] = useState([0, 0])
    page = page < 0 ? imgs.length + page : page;
    const imageIndex = page % imgs.length;
    const x = useMotionValue(0)
    const left = useTransform(x, [-1, 0, 1], [1, 0, -1], {clamp: false})
    const rotateY = useTransform(x, [500, 0, -500], [90, 0, -90])
    const sliding = useRef(true)
    const beforeImg = imageIndex ? imageIndex - 1 : imgs.length - 1
    const afterImg = (imageIndex + 1) % imgs.length
    const buttonsStyle = {
        backgroundColor: "rgba(100,100,255,0.1)",
        fontSize: "10vw",
        color: "darkblue",
        margin: 0,
        boxShadow: "0 0 10px rgba(0,0,255,0.5)",
        overflow: "hidden"
    }
    const paginate = (newDirection) => {
        if (sliding.current) {
            setPage([page + newDirection, newDirection])
        }
        sliding.current = false
    }

    useEffect(() => {
        if (!rotateY.get() || Math.abs(rotateY.get()) === 90)
            x.jump(0)
    }, [page, direction])

    return (
        <motion.div
            style={{
                perspective: 1200,
                position: "fixed",
                top: 0,
                left: 0,
                zIndex: 1111,
                backgroundColor: "rgba(0,0,0,0.5)",
                width: "100%",
                height: "100%",
                display: "grid",
                justifyContent: "center",
                alignItems: "center",
                gridTemplateColumns: "auto auto auto"
            }}
            initial={{
                clipPath: `circle(0% at ${mouseEvent.clientX}px ${mouseEvent.clientY}px)`,
            }}
            animate={{
                clipPath: "circle(71%)"
            }}
            exit={{
                clipPath: `circle(0% at ${mouseEvent.clientX}px ${mouseEvent.clientY}px)`,
            }}
        >
            <div
                style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: "100%",
                }}
                onClick={() => setCertificate(false)}
            >

            </div>
            <motion.button
                onClick={() => paginate(-1)}
                style={{
                    borderRight: "none",
                    borderBottomLeftRadius: "50%",
                    borderTopLeftRadius: "50%",
                    ...buttonsStyle
                }}
                title={"Preview"}
                whileHover={{
                    backgroundColor: "rgba(93,239,245,0.5)",
                }}
            >
                <FontAwesomeIcon icon={faChevronLeft} beat width={"10vw"}/>
            </motion.button>
            <motion.article
                style={{
                    position: "relative",
                    overflow: "hidden",
                }}
            >
                <motion.div
                    style={{
                        position: "absolute",
                        x,
                        left,
                        height: "100%",
                        width: "100%",
                        zIndex: 10
                    }}
                    drag={"x"}
                    dragSnapToOrigin={true}
                    onDragEnd={(event, info) => {
                        if (rotateY.get() > 45 || rotateY.get() < -45) {
                            sliding.current = true
                            x.stop()
                            info.offset.x > 0 ? paginate(-1) : paginate(1)
                        }
                    }}
                >
                </motion.div>
                <motion.div
                    style={{
                        position: "relative",
                        transformStyle: "preserve-3d",
                        transformOrigin: `center center -35vw`,
                        width: "70vw",
                        display: "flex",
                        justifyContent: "center",
                        rotateY,
                    }}
                    custom={{direction, rotateY: rotateY.get()}}
                    variants={variants}
                    initial="enter"
                    animate="center"
                    key={imageIndex}
                    onAnimationStart={() => {
                        sliding.current = false
                    }}
                    onAnimationComplete={() => {
                        x.jump(0)
                        sliding.current = true
                    }}
                >
                    <motion.img
                        style={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                            rotateY: 90,
                            translateX: "100%",
                            transformOrigin: `left center`,
                        }}
                        className={"img-fluid"}
                        src={imgs[beforeImg]}
                        loading={"lazy"}
                    />
                    <motion.img
                        style={{
                            position: "relative",
                        }}
                        className={"img-fluid"}
                        src={imgs[imageIndex]}
                        transition={{
                            rotateY: {duration: 0.5},
                        }}
                        loading={"lazy"}
                    />
                    <div
                        style={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                            transform: "rotateY(90deg)",
                            transformOrigin: `left center`,
                            width: "100%",
                            height: "100%"
                        }}
                    >
                        <motion.img
                            style={{
                                scaleX: -1,
                            }}
                            className={"img-fluid"}
                            src={imgs[afterImg]}
                            loading={"lazy"}
                        />
                    </div>
                </motion.div>
            </motion.article>
            <motion.button
                onClick={() => paginate(1)}
                style={{
                    borderLeft: "none",
                    borderBottomRightRadius: "50%",
                    borderTopRightRadius: "50%",
                    ...buttonsStyle
                }}
                title={"Next"}
                whileHover={{
                    backgroundColor: "rgba(93,239,245,0.5)",
                }}
            >
                <FontAwesomeIcon icon={faChevronRight} beat width={"10vw"}/>
            </motion.button>
        </motion.div>
    )
}