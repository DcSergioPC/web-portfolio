import {useEffect, useRef, useState} from "react";
import {AnimatePresence, motion, useCycle, useMotionValue, useTransform, animate} from "framer-motion";
import ListProjects from "./GetProjects.jsx";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAnglesUp, faChevronRight, faQuestion} from "@fortawesome/free-solid-svg-icons";
import {useTranslation} from "react-i18next";

export default function Project() {

    const x = useMotionValue(0)
    const y = useMotionValue(0)
    const left = useTransform(x, [-1, 0, 1], [1, 0, -1], {clamp: false})
    const top = useTransform(y, [-1, 0, 1], [1, 0, -1], {clamp: false})
    const yInput = [600, 0, -600]
    const rotateX = useTransform(y, [-600, 0, 600], [90, 0, -90])
    const xInput = [1500, 0, -1500]
    const rotateY = useTransform(x, xInput, ['10deg', '0deg', '-10deg'])
    const [pjInd, setPjInd] = useState(0)
    const {i18n} = useTranslation()
    useEffect(() => {
        animate(rotateX, [180, 0], {type: "spring", stiffness: 50, damping: 7})
    }, [])

    useEffect(() => {
        window.y = y
        window.animate = animate
        window.rotateX = rotateX
        window.pjInd = pjInd
        window.setPjInd = setPjInd
    });

    useEffect(() => {
        const rotateMiddle = y.get()
        const toInitialValues = () => {
            top.jump(0)
            y.jump(0)
        }
        if (rotateMiddle < yInput[2] || rotateMiddle > yInput[0]) {
            toInitialValues()
        } else {
            const transition = {type: "spring", stiffness: 200, damping: 10, duration: 2}
            if (rotateMiddle === yInput[0]) {
                animate(y, [yInput[2], 0], transition)
            } else if (rotateMiddle === yInput[2]) {
                animate(y, [yInput[0], 0], transition)
            } else if (rotateMiddle < 0) {
                animate(y, [yInput[0] + rotateMiddle, 0], transition)
                    .then(toInitialValues)
            } else if (rotateMiddle > 0) {
                animate(y, [yInput[2] + rotateMiddle, 0], transition)
                    .then(toInitialValues)
            }
        }
    }, [pjInd])

    const initial = {
        height: "60vh",
        width: "60vw",
        borderRadius: "var(--bs-border-radius-xxl)"
    }

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        overflowX: "visible",
        perspective: "1200px",
        zIndex: 1104,
        transform: 'translate(-50%,-50%)',
        userSelect: "none",
        MozUserSelect: "none",
        WebkitUserSelect: "none"
    }

    let draggin = false

    const dragStyle = {
        x,
        y,
        top,
        left
    }

    function handleDragEnd(event, {velocity, offset}) {
        draggin = false
        const rotateMiddle = y.get()
        const speedBound = 250000
        if (rotateMiddle < yInput[2] / 2 || velocity.y * offset.y > speedBound) {
            y.stop()
            setPjInd((pjInd + 1) % ListProjects.length)
        } else if (rotateMiddle > yInput[0] / 2 || velocity.y * offset.y > speedBound) {
            y.stop()
            setPjInd(pjInd === 0 ? ListProjects.length - 1 : pjInd - 1)
        }
    }

    function autoComplete(frame, type, value) {
        const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
        const input = frame.contentDocument.querySelector(`[type="${type}"]`)
        if (!input)
            return
        nativeInputValueSetter.call(input, value);
        const event = new Event("input", {bubbles: true});
        input.dispatchEvent(event);
    }

    const Arrow = (props) => (
        <svg width={14} height={7} style={{transform: 'rotateZ(180deg)'}} {...props}>
            <g fill="none" fillRule="evenodd">
                <path
                    fill="#CDCFD0"
                    d="M7 .07v1.428l-5.55 5.5L0 6.982zM7 .07v1.428l5.55 5.5L14 6.982z"
                />
                <path fill="#FFF" d="M1.45 7L7 1.498 12.55 7z"/>
            </g>
        </svg>
    );
    const HelpCircle = ({frameRef, info = ListProjects[pjInd].info}) => {
        const [onHover, setOnHover] = useCycle(false, true)
        if (!info)
            return <></>

        function popUp(status) {
            const userAgent = window.navigator.userAgent.toLowerCase()
            const isMobile = /iphone|ipad|ipod|android/i.test(userAgent)
            if (isMobile && status === "hover")
                return
            else if (status !== "hover" && !isMobile)
                return
            setOnHover()
        }

        return (
            <motion.aside
                className={"position-absolute rounded-circle bg-white d-flex justify-content-center align-items-center"}
                initial={{scale: 0}}
                animate={{scale: 1, transition: {type: "spring", stiffness: 1000}}}
                exit={{scale: 0}}
                style={{
                    width: "70px",
                    height: "70px",
                    bottom: "4%",
                    left: "3%",
                    transformStyle: 'preserve-3d',
                    perspective: '100px',
                    boxShadow: '1px 1px 10px 1px black'
                }}
                href={ListProjects[pjInd].repository}
                target={"new"}
                onHoverStart={() => popUp("hover")}
                onHoverEnd={() => popUp("hover")}
                onClick={popUp}
                whileHover={{
                    scale: 1.2
                }}
            >
                <AnimatePresence>
                    {onHover &&
                        <motion.div
                            style={{
                                position: 'absolute',
                                bottom: '100%',
                                userSelect: 'text',
                                msUserSelect: 'text',
                                WebkitUserSelect: 'text'
                            }}
                            initial={{
                                scale: 0,
                                opacity: 0
                            }}
                            animate={{
                                scale: 1,
                                opacity: 1
                            }}
                            exit={{
                                scale: 0,
                                opacity: 0
                            }}
                        >
                            <motion.div
                                className={'bg-white text-bg-light p-1 rounded-3 text-end'}
                                style={{
                                    boxShadow: '1px 1px 10px black, -1px -1px 1px black'
                                }}
                            >
                                <table>
                                    {info.map(e =>
                                        <tbody key={e.email}>
                                        <tr>
                                            <td
                                                onClick={() => {
                                                    autoComplete(frameRef.current, "email", e.email)
                                                    autoComplete(frameRef.current, "password", e.password)
                                                }}
                                                className={"btn btn-outline-primary text-nowrap py-0 ps-0 me-1"}
                                            >
                                                <FontAwesomeIcon icon={faChevronRight} color={"red"} beat
                                                                 className={"px-1"}/>
                                                <strong> email:</strong>
                                            </td>
                                            <td>
                                                {e.email}
                                            </td>
                                        </tr>
                                        <tr key={e.email + "p"}>
                                            <td><strong>password:</strong></td>
                                            <td> {e.password}</td>
                                        </tr>
                                        </tbody>
                                    )}
                                </table>
                            </motion.div>
                            <div className={"d-flex justify-content-center"}><Arrow/></div>
                        </motion.div>
                    }
                </AnimatePresence>
                <motion.div
                    style={{
                        transformOrigin: 'center center center',
                        translateZ: 10
                    }}
                    initial={{
                        rotateY: 0,
                    }}
                    animate={{
                        rotateY: [0, 360]
                    }}
                    transition={{
                        repeat: Infinity,
                        repeatType: 'reverse',
                        duration: 1
                    }}
                >
                    <FontAwesomeIcon icon={faQuestion} size={"3x"}/>
                </motion.div>
            </motion.aside>
        )
    }
    const ComponentProject = ({pjInd, highLoad = false, transform = ''}) => {
        const [scale, setScale] = useState(false)
        const [frameLoad, setFrameLoad] = useCycle(true, false)
        const frameRef = useRef(null)
        let source = ListProjects[pjInd].url

        useEffect(() => {
            if (scale)
                animate(y, 0)
            console.log(ListProjects[pjInd].title[i18n.language])
        }, [scale])

        useEffect(() => {
            if (frameRef && !highLoad && !frameLoad && ListProjects[pjInd].info && frameRef.current.contentDocument) {
                autoComplete(frameRef.current, "email", ListProjects[pjInd].info[0].email)
                autoComplete(frameRef.current, "password", ListProjects[pjInd].info[0].password)
            }
        }, [frameLoad])

        const animate2 = scale
            ?
            {
                height: '100vh',
                width: '100vw',
                borderRadius: 0
            }
            :
            {}

        if (highLoad) {
            source = null
        }
        return (
            <main
                style={{
                    position: transform === '' ? 'relative' : 'absolute',
                    top: 0,
                    left: 0,
                    transform: transform,
                    backfaceVisibility: "hidden"
                }}
            >
                <motion.div
                    style={{
                        transformStyle: "preserve-3d",
                        transformOrigin: "center center -800px"
                    }}
                >
                    <AnimatePresence>
                        {scale && <motion.button className={"btn btn-outline-primary mt-2 ms-2 position-absolute z-1"}
                                                 onClick={() => setScale(false)}
                                                 initial={{translateX: "-120%"}}
                                                 animate={{translateX: "0%"}}
                                                 exit={{translateX: "-120%", opacity: 0}}
                        >Back</motion.button>}
                    </AnimatePresence>
                    <motion.section
                        initial={initial}
                        animate={animate2}
                        style={{
                            backgroundColor: !frameLoad ? "transparent" : "black",
                            overflow: 'hidden',
                            boxShadow: " 0px 0px 50px 10px black"
                        }}
                    >
                        {frameLoad ?
                            <div
                                className={"position-absolute top-0 d-flex justify-content-center align-items-center w-100 h-100"}
                                style={{borderRadius: "20px"}}>
                                <div className="spinner-border text-primary"
                                     style={{width: "10rem", height: "10rem", borderWidth: "1rem"}} role="status">
                                    <span className="sr-only"></span>
                                </div>
                            </div>
                            :
                            <AnimatePresence>
                                {!scale &&
                                    <>
                                        <motion.section
                                            className={"position-absolute end-0 p-1 pe-0"}
                                            style={{zIndex: 100, top: "30px", overflow: "hidden"}}
                                        >
                                            <motion.div
                                                style={{
                                                    boxShadow: '0px 0px 10px black'
                                                }}
                                                initial={{transform: "translateX(100%)"}}
                                                animate={{transform: "translateX(0%)"}}
                                                exit={{transform: "translateX(100%)"}}
                                                className={"container rounded-start-5 p-2 ps-3 bg-body-secondary"}
                                            >
                                                {ListProjects[pjInd].technologies.map((tech, ind) => (
                                                    <motion.img
                                                        key={ind + tech}
                                                        className={"mx-1"}
                                                        src={`https://skillicons.dev/icons?i=${tech}`}
                                                        alt={tech}
                                                        title={tech[0].toUpperCase() + tech.slice(1)}
                                                        whileHover={{
                                                            scale: 1.2
                                                        }}
                                                    />
                                                ))}
                                            </motion.div>
                                        </motion.section>
                                    </>
                                }
                            </AnimatePresence>
                        }
                        <motion.iframe
                            scrolling={!scale ? "no" : "auto"}
                            initial={{
                                transform: "translate(-50%, -50%) scale(0.6)",
                            }}
                            animate={scale
                                ?
                                {
                                    transform: "translate(-50%, -50%) scale(1)"
                                }
                                :
                                {
                                    transform: "translate(-50%, -50%) scale(0.6)"
                                }
                            }
                            style={{
                                height: "100vh",
                                width: "100vw",
                                position: 'relative',
                                visibility: frameLoad ? "hidden" : "visible",
                                top: '50%',
                                left: '50%'
                            }}
                            onLoad={setFrameLoad}
                            src={source}
                            ref={frameRef}
                        ></motion.iframe>
                        <AnimatePresence>
                            {!scale &&
                                <div
                                    style={{

                                        top: "0%",
                                        width: "100%",
                                        height: "100%"
                                    }}
                                    className={"position-absolute"}
                                >
                                    <motion.div
                                        style={
                                            transform === ''
                                                ?
                                                {
                                                    ...dragStyle,
                                                    width: "100%",
                                                    height: "100%",
                                                }
                                                :
                                                {}
                                        }
                                        className={"position-relative"}
                                        animate={{top: '0%'}}
                                        exit={{top: "-100%", opacity: 0}}
                                        drag
                                        dragSnapToOrigin={true}
                                        dragTransition={{bounceDamping: 10, bounceStiffness: 600}}
                                        onDragStart={() => draggin = true}
                                        onDragEnd={handleDragEnd}
                                        onClick={() => {
                                            if (!draggin) {
                                                setScale(true)
                                            }
                                        }}
                                    >
                                        <motion.small
                                            className={"position-relative container d-flex justify-content-center text-center"}
                                            style={{
                                                transform: "translateY(-100%)",
                                                textShadow:
                                                    " 5px 5px 5px black," +
                                                    "-5px 5px 5px black," +
                                                    " 5px -5px 5px black," +
                                                    "-5px -5px 5px black"
                                            }}
                                        >
                                            <div>
                                                <h2 style={{fontSize: "max(1em,1.5vw)"}}>
                                                    {ListProjects[pjInd].title[i18n.language]}
                                                </h2>
                                                <h4 style={{fontWeight: 100, fontSize: "max(0.7em,1.5vw)"}}>
                                                    {ListProjects[pjInd].description[i18n.language]}
                                                </h4>
                                            </div>
                                        </motion.small>
                                    </motion.div>
                                </div>
                            }
                        </AnimatePresence>
                        <AnimatePresence>
                            {!scale && !frameLoad &&
                                <motion.a
                                    className={"position-absolute rounded-circle bg-primary d-flex justify-content-center align-items-center"}
                                    initial={{scale: 0}}
                                    animate={{scale: 1, transition: {type: "spring", stiffness: 1000}}}
                                    exit={{scale: 0}}
                                    style={{
                                        width: "70px",
                                        height: "70px",
                                        bottom: "4%",
                                        right: "3%",
                                        boxShadow: '1px 1px 10px 1px black'
                                    }}
                                    href={ListProjects[pjInd].repository}
                                    target={"new"}
                                    whileHover={{
                                        scale: 1.2
                                    }}
                                >
                                    <motion.img
                                        animate={{
                                            rotateZ: [-20, 20, 0, -20, 0, 10, 0, 10, 0],
                                            transition: {repeat: Infinity, repeatDelay: 2, stiffness: 1000}
                                        }}
                                        className={"position-relative h-75 w-75"}
                                        src={"https://skillicons.dev/icons?i=gitlab"}
                                        alt={"gitlab"}
                                        title={"Gitlab"}
                                    />
                                </motion.a>
                            }
                        </AnimatePresence>
                        <AnimatePresence>
                            {!scale && !frameLoad &&
                                <HelpCircle frameRef={frameRef}/>
                            }
                        </AnimatePresence>
                    </motion.section>
                </motion.div>
                <motion.aside
                    style={{
                        position: "absolute",
                        top: "110%",
                        width: "100%"
                    }}
                    className={"d-flex justify-content-center"}
                >
                    <motion.div
                        style={{
                            cursor: "pointer"
                        }}
                        animate={{
                            translateY: [0, 20]
                        }}
                        transition={{
                            duration: 0.4,
                            repeat: Infinity,
                            ease: "backIn"
                        }}
                        onClick={() => {
                            y.jump(yInput[2])
                            setPjInd((pjInd + 1) % ListProjects.length)
                        }}
                    >
                        <FontAwesomeIcon icon={faAnglesUp} size={"4x"}/>
                    </motion.div>
                </motion.aside>
            </main>
        )
    }
    return (
        <>
            <main
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100vw',
                    height: '100vh',
                    overflow: 'hidden'
                }}
                onWheel={(event) => {
                    if (event.deltaY < 0) {
                        y.jump(yInput[2])
                        setPjInd((pjInd + 1) % ListProjects.length)
                    } else if (event.deltaY > 0) {
                        y.jump(yInput[0])
                        setPjInd(pjInd === 0 ? ListProjects.length - 1 : pjInd - 1)
                    }
                }}
            >
                <motion.aside
                    initial={{
                        opacity: 0,
                    }}
                    animate={{
                        opacity: 1
                    }}
                    exit={{opacity: 0, height: "0px"}}
                    style={style}
                >
                    <motion.section
                        style={{
                            transformStyle: 'preserve-3d',
                            transformOrigin: 'center center -50vh',
                            rotateX,
                            rotateY,
                        }}
                    >
                        <ComponentProject pjInd={pjInd === 0 ? ListProjects.length - 1 : pjInd - 1} highLoad={true}
                                          transform={'rotateX(90deg) translateY(-50vh) translateZ(50vh)'}/>
                        <ComponentProject pjInd={pjInd}/>
                        <ComponentProject pjInd={(pjInd + 1) % ListProjects.length} highLoad={true}
                                          transform={'rotateX(-90deg) translateY(50vh) translateZ(50vh)'}/>
                    </motion.section>
                </motion.aside>
            </main>
        </>
    )
}