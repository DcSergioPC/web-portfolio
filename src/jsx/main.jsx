import React from 'react'
import ReactDOM from 'react-dom/client'
import {
    Route,
    createBrowserRouter,
    createRoutesFromElements,
    RouterProvider
} from 'react-router-dom'
// Import our custom CSS
import '../scss/styles.scss'

import App from "./App.jsx";

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path={'/*'} element={<App/>}/>
    ),
    {
        basename: '/web-portfolio/'
    }
)
ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <RouterProvider router={router}/>
    </React.StrictMode>,
)
