import {motion} from "framer-motion";
import {Link, useResolvedPath} from "react-router-dom";
import {useTranslation} from "react-i18next";
import Lang from "./Lang.jsx";

export default function Navbar() {
    const [t, _] = useTranslation()
    const relativePath = useResolvedPath()
    const select = relativePath.pathname.replaceAll('/', '')
    return (
        <>
            <nav className="navbar navbar-expand sticky-top" style={{userSelect: "none"}} data-bs-theme="dark">
                <div className="collapse navbar-collapse position-relative" id="navbarColor02">
                    <div
                        style={{
                            position: "absolute",
                            display: "flex",
                            top: "0px",
                            left: "0px",
                            width: "100%",
                            flexDirection: "row",
                            flexWrap: "wrap",
                            alignItems: "center"
                        }}
                    >
                        <div
                            style={{
                                flexGrow: 0.98,
                                minWidth: "270px",
                                height: "40px"
                            }}
                        >

                        </div>
                        <Lang/>
                    </div>
                    <ul className="navbar-nav mx-auto mb-2 mb-lg-0 z-1">
                        <li className="nav-item">
                            <Link to={'/'} style={{textDecoration: 'none'}}>
                                <motion.aside
                                    whileHover={{
                                        scale: 1.3
                                    }}
                                    className={`nav-link ${select === "" ? "active" : ""}`}
                                    aria-current="page">{t('nav.home')}
                                </motion.aside>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/projects'} style={{textDecoration: 'none'}}>
                                <motion.aside
                                    whileHover={{
                                        scale: 1.2
                                    }}
                                    className={`nav-link ${select === "projects" ? "active" : ""}`}>{t('nav.projects')}
                                </motion.aside>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/about'} style={{textDecoration: 'none'}}>
                                <motion.aside
                                    whileHover={{
                                        scale: 1.3
                                    }}
                                    className={`nav-link ${select === "about" ? "active" : ""}`}>{t('nav.about')}
                                </motion.aside>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}