import {
    Route, Routes, useLocation,
} from "react-router-dom";
import {lazy, Suspense, useEffect, useRef} from "react";
import {AnimatePresence} from "framer-motion";

const ProfileCard = lazy(() => import("./ProfileCard.jsx"));
const InfoCard = lazy(() => import("./InfoCard.jsx"));
import Navbar from "./Navbar.jsx";
import Background from "./Background.jsx";

const Project = lazy(() => import("./Project.jsx"));
import {I18nextProvider} from "react-i18next"
import i18next from "i18next";
import esES from "../public/locales/es/translation.json"
import enUS from "../public/locales/en/translation.json"

i18next.init({
    interpolation: {escapeValue: false},
    lng: localStorage.getItem('lang') ? localStorage.getItem('lang') : navigator.language.startsWith("es") ? "es" : "en",
    resources: {
        es: {
            translation: esES,
        },
        en: {
            translation: enUS
        }
    },
    fallbackLng: "en"
})
const App = () => {
    const locate = useLocation()
    const firstRender = useRef(false)

    useEffect(() => {
        firstRender.current = true
    }, [])
    return (
        <I18nextProvider i18n={i18next}>
            <Background/>
            <Navbar/>
            <AnimatePresence>
                {
                    !location.pathname.includes('projects') &&
                    <ProfileCard firstRender={firstRender}/>
                }
                <Routes location={locate} key={locate.pathname}>
                    <Route path={'/'} element={<Suspense fallback={null}><InfoCard/></Suspense>}/>
                    <Route path={'/about'} element={<></>}/>
                    <Route path={'/projects'} element={<Suspense fallback={null}><Project/></Suspense>}/>
                </Routes>
            </AnimatePresence>
        </I18nextProvider>
    )
}

export default App;

