import {AnimatePresence, motion, animate, useMotionValue} from "framer-motion";
import SkillBox from "./SkillBox.jsx";
import {lazy, Suspense, useRef, useState} from "react";
import {useTranslation} from "react-i18next";

const SliderImg = lazy(() => import("./SliderImg.jsx"))

const InfoCard = () => {
    const {t} = useTranslation()
    const [mouseEvent, setMouseEvent] = useState(null)
    const [certificate, setCertificate] = useState("")
    const animated = useRef(true)
    const [certificatesSegment, languagesSegment] = [useMotionValue("0%"), useMotionValue("0%")]
    const container = {
        hidden: {opacity: 1, scale: 0}, visible: {
            opacity: 1, scale: 1, transition: {
                delayChildren: 0.05, staggerChildren: 0.1
            }
        }
    };

    const item = {
        hidden: {y: 30, opacity: 0}, visible: {
            y: 0, opacity: 1
        }
    };

    const languageContainer = {
        hidden: {opacity: 1, scale: 0}, visible: {
            opacity: 1, scale: 1, transition: {
                delayChildren: 0.3, staggerChildren: 0.3
            }
        }
    };

    const language = {
        hidden: {y: "-150%", opacity: 0}, visible: {
            y: 0, opacity: 1, transition: {
                duration: 0.3
            }
        }
    };
    return (
        <>
            <Suspense fallback={null}>
                <AnimatePresence>
                    {certificate === "english" &&
                        <SliderImg setCertificate={setCertificate} mouseEvent={mouseEvent}
                                   imgs={["english/englishCertificateLvl2.webp", "english/englishCertificateLvl1.webp"]}/>
                    }
                    {certificate === "bootcamp" &&
                        <SliderImg setCertificate={setCertificate} mouseEvent={mouseEvent}
                                   imgs={["bootcamp/front.webp", "bootcamp/back.webp"]}/>
                    }
                </AnimatePresence>
            </Suspense>
            <motion.section
                className={"overflow-hidden"}
                exit={{opacity: 0, height: 0}}>
                {/*Skills*/}
                <motion.div className={"container"}
                            variants={container}
                            initial="hidden"
                            animate="visible">
                    <div className={""}>
                        <h1 className={"mt-4"}>{t('info.skills.title')}</h1>
                    </div>
                    <div className={"mb-3 scale-left"} style={{borderBottom: "5px solid"}}>

                    </div>
                    <div className={"grid"}>
                        <div className="row justify-content-center text-center mt-3">
                            {import.meta.env.VITE_SKILLS.split(",").map((e) => (
                                <motion.div key={e} className="col-xxl-3 col-lg-4 col-md-6 col-12" variants={item}
                                            whileHover={{
                                                scale: 1.2,
                                                zIndex: 1
                                            }}
                                >
                                    <SkillBox
                                        url={`https://skillicons.dev/icons?i=${e !== "PosgreSQL" ? e.toLowerCase() : "postgres"}`}
                                        name={e}/>
                                </motion.div>))}
                        </div>
                    </div>
                </motion.div>

                {/*Studies*/}
                <motion.div className={"container"}
                            initial={animated.current ? {opacity: 0, translateY: 200} : {}}
                            whileInView={animated.current ? {
                                opacity: 1,
                                translateY: 0,
                                transition: {ease: "easeInOut", type: 'spring', stiffness: 200, damping: 15}
                            } : {}}
                            viewport={{once: true, margin: "-50px"}}
                >
                    <div className={""}>
                        <h1 className={"mt-4"}>{t('info.studies.title')}</h1>
                    </div>
                    <motion.div
                        className={"mb-3 scale-left"}
                        style={{
                            borderBottom: "5px solid",
                            width: certificatesSegment,
                            boxShadow: "0 0 20px 6px darkblue"
                        }}
                        onViewportEnter={() => {
                            if (certificatesSegment.get() === "0%")
                                animate(certificatesSegment, ["0%", "100%"], {duration: 2.5, ease: "easeOut"})
                        }}
                    ></motion.div>
                    <motion.div onClick={(e) => {
                        e.preventDefault()
                        setMouseEvent(e)
                        setCertificate("bootcamp")
                    }} className={"container btn btn-outline-primary border-0"} style={{overflow: "hidden"}}
                                variants={languageContainer}
                                initial="hidden"
                                animate="visible"
                                whileHover={{
                                    scale: 1.1
                                }}
                    >
                        <motion.div className="row text-center"
                                    variants={language}>
                            <div className={"col-12 col-md-auto mx-md-0 mx-auto"}><img src="bootcamp/bootcamp.png"
                                                                                       width={"100px"}
                                                                                       alt={"Bootcamp Logo"}/></div>
                            <div className={"col-auto align-self-center mt-2 mt-md-0 mx-auto mx-md-0"}><h4
                                style={{textShadow: "0.5px 0.5px 0 black, 0.5px -1.5px 0 black, -0.5px 0.5px 0 black, -0.5px -1.5px 0 black, 0.5px 0px 0 black, 0px 0.5px 0 black, -0.5px 0px 0 black, 0px -0.5px 0 black"}}>
                                <b>{t('info.studies.bootcamp.title')}:</b> {t('info.studies.bootcamp.description')}</h4>
                            </div>
                        </motion.div>
                    </motion.div>
                </motion.div>

                {/*Languages*/}
                <motion.div className={"container"}
                            initial={animated.current ? {opacity: 0, translateY: 200} : {}}
                            whileInView={animated.current ? {
                                opacity: 1,
                                translateY: 0,
                                transition: {ease: "easeInOut", type: 'spring', stiffness: 200, damping: 15}
                            } : {}}
                            viewport={{once: true, margin: "-50px"}}
                            onViewportEnter={() => {
                                animated.current = false
                            }}
                >
                    <div className={""}>
                        <h1 className={"mt-4"}>{t('info.languages.title')}</h1>
                    </div>
                    <motion.div
                        className={"mb-3"}
                        style={{
                            borderBottom: "5px solid",
                            width: languagesSegment,
                            boxShadow: "0 0 20px 6px darkblue"
                        }}
                        onViewportEnter={() => {
                            if (languagesSegment.get() === "0%")
                                animate(languagesSegment, ["0%", "100%"], {duration: 2.5, ease: "easeOut"})
                        }}
                    ></motion.div>
                    <motion.div className={"container"} style={{overflow: "visible"}}
                                variants={languageContainer}
                                initial="hidden"
                                animate="visible"
                    >
                        <div onClick={(e) => {
                            e.preventDefault()
                            setMouseEvent(e)
                            setCertificate("english")
                        }} className={"container mt-3 btn btn-outline-primary border-0"}>
                            <motion.div className="row text-center"
                                        variants={language}
                                        whileHover={{
                                            scale: 1.1
                                        }}
                            >
                                <div className={"col-12 col-md-auto mx-md-0 mx-auto"}><img src="english/english.png"
                                                                                           width={"100px"}
                                                                                           alt={"english logo"}
                                /></div>
                                <div className={"col-auto align-self-center mt-2 mt-md-0 mx-auto mx-md-0"}><h4
                                    style={{textShadow: "0.5px 0.5px 0 black, 0.5px -1.5px 0 black, -0.5px 0.5px 0 black, -0.5px -1.5px 0 black, 0.5px 0px 0 black, 0px 0.5px 0 black, -0.5px 0px 0 black, 0px -0.5px 0 black"}}>
                                    <b>{t('info.languages.english.title')}:</b> {t("info.languages.english.description")}
                                </h4></div>
                            </motion.div>
                        </div>
                        <div className={"overflow-hidden container"}>
                            <motion.div className="row text-center mt-3"
                                        variants={language}
                            >
                                <div className={"col-12 col-md-auto mx-md-0 mx-auto"}><img src="spain.png"
                                                                                           width={"100px"}
                                                                                           alt={"spain flag"}
                                /></div>
                                <div className={"col-auto align-self-center mt-2 mt-md-0 mx-auto mx-md-0"}><h4>
                                    <b>{t('info.languages.spanish.title')}:</b> {t('info.languages.spanish.description')}
                                </h4></div>
                            </motion.div>
                        </div>
                    </motion.div>
                </motion.div>
            </motion.section>
        </>
    );
};

export default InfoCard;