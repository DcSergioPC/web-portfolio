import {AnimatePresence, motion} from "framer-motion";
import {useLocation} from "react-router-dom";
import {useTranslation} from "react-i18next";


const ProfileCard = ({firstRender}) => {
    const {t} = useTranslation()
    const location = useLocation()
    const home = location.pathname === '/'
    const about = location.pathname.includes('about')
    const firstColor = '#0000FF';
    const secondColor = '#7FFFF4'
    return (
        <>
            <motion.main className={`container-fluid mt-lg-5`}
                         style={{overflowX: "clip"}}
                         initial={{y: "-100%"}}
                         animate={{y: "0%"}}
                         exit={{translateY: "-100%", opacity: 0}}>
                <div className={""}
                     style={{
                         display: "flex",
                         flexWrap: "wrap",
                         justifyContent: "center",
                         alignItems: "center"
                     }}
                >
                    <motion.span className={"col-12 col-md-4 justify-self-center align-self-center"}
                                 style={{
                                     aspectRatio: 1
                                 }}
                                 animate={about ? {
                                     width: "80vh", transition: {
                                         duration: 0.5
                                     }
                                 } : {width: "max(21vw,42vh)"}}><img id={"myphoto"}
                                                                     className={"mx-auto ms-md-auto img-fluid rounded-circle"}
                                                                     src="photo.webp"
                                                                     alt="my personal photo"
                                                                     title="DcSergio"
                                                                     style={{filter: `drop-shadow(0px 0px 10px var(--bs-body-color))`}}
                    />
                    </motion.span>

                    <div id={!firstRender.current ? "mydescription" : ""}
                         className={"col col-md-6 justify-self-center text-center fw-semibold"}
                         style={{
                             minWidth: "210px",
                             zIndex: 1
                         }}
                    >
                        <motion.h1
                            style={{
                                backgroundClip: "text",
                                WebkitBackgroundClip: "text",
                                backgroundSize: "100% 200%",
                                color: "transparent"
                            }}
                            initial={{
                                backgroundImage: `linear-gradient(${firstColor}, ${secondColor}, ${firstColor})`
                            }}
                            animate={{
                                backgroundPositionY: ["0%", "200%"],
                                transition: {
                                    repeat: Infinity,
                                    repeatType: "loop",
                                    ease: "linear",
                                    duration: 10,
                                }
                            }}
                        ><b>Sergio Portillo</b></motion.h1>
                        <br/>
                        <AnimatePresence>
                            {home && <motion.div key={"home"} className={"overflow-hidden"}
                            >
                                <motion.div
                                    initial={{height: 0, textWrap: "balance", margin: "auto", maxWidth: "422px"}}
                                    animate={{height: "auto"}}
                                    exit={{opacity: 0, height: 0}}>
                                    {t('profile.description')}
                                </motion.div>
                            </motion.div>}
                        </AnimatePresence>
                        <AnimatePresence>
                            {about && <>
                                <motion.div key={"about"} className={"overflow-hidden"}
                                            initial={{
                                                opacity: 0, height: 0,
                                                origin: "50%", display: "none"
                                            }}
                                            animate={{
                                                opacity: 1,
                                                height: "auto",
                                                origin: "50%"
                                            }}
                                            exit={{
                                                height: 0,
                                                margin: "0px",
                                                padding: "0px",
                                                transition: {duration: 0.3}
                                            }}>
                                    {t('profile.about')}
                                </motion.div>
                                {/*LinkedIn and Repository*/}
                                <motion.section
                                    initial={{opacity: 0, transform: "translateX(50%)", paddingTop: "0px", height: 0}}
                                    animate={{
                                        opacity: 1,
                                        transform: "translateX(0%)",
                                        transition: {type: "spring"},
                                        height: 78,
                                        paddingTop: "30px"
                                    }}
                                    exit={{opacity: 0, height: 0, paddingTop: "0px", transform: "translateX(100%)"}}
                                    className={"grid"}>
                                    <a className={"mx-3"} href={"https://www.linkedin.com/in/dcsergio/"} target="new">
                                        <motion.img
                                            src={"https://skillicons.dev/icons?i=linkedin"}
                                            alt={"linkedin"}
                                            title={"LinkedIn"}
                                            initial={{
                                                filter: "drop-shadow(0px 0px 5px blue)"
                                            }}
                                            whileHover={{
                                                scale: 1.2,
                                                filter: "drop-shadow(0px 0px 20px blue)"
                                            }}
                                        />
                                    </a>
                                    <a className={"mx-3"} href={"https://gitlab.com/DcSergioPC/"} target="new">
                                        <motion.img
                                            src={"https://skillicons.dev/icons?i=gitlab"}
                                            alt={"gitlab"}
                                            title={"Gitlab"}
                                            initial={{
                                                filter: "drop-shadow(0px 0px 5px blue)"
                                            }}
                                            whileHover={{
                                                scale: 1.2,
                                                filter: "drop-shadow(0px 0px 20px blue)"
                                            }}
                                        />
                                    </a>
                                    <a className={"mx-3"} href={"mailto:portillo.daniel12@gmail.com"} target="new">
                                        <motion.img
                                            src={"gmail.png"} style={{width: "48px", height: "48px"}}
                                            alt={"gmail"}
                                            title={"portillo.daniel12@gmail.com"}
                                            initial={{
                                                filter: "drop-shadow(0px 0px 5px blue)"
                                            }}
                                            whileHover={{
                                                scale: 1.2,
                                                filter: "drop-shadow(0px 0px 20px blue)"
                                            }}
                                        />
                                    </a>
                                    <a className={"mx-3"} href={"https://github.com/DcSergioPC"} target="new">
                                        <motion.img
                                            src={"https://skillicons.dev/icons?i=github"}
                                            alt={"Github"}
                                            title={"Github"}
                                            initial={{
                                                filter: "drop-shadow(0px 0px 5px blue)"
                                            }}
                                            whileHover={{
                                                scale: 1.2,
                                                filter: "drop-shadow(0px 0px 20px blue)"
                                            }}
                                        />
                                    </a>
                                </motion.section>
                            </>}
                        </AnimatePresence>
                    </div>
                </div>
            </motion.main>
        </>
    );
};

export default ProfileCard;