import {motion} from "framer-motion"

const cardVariants = {
    offscreen: {
        x: "-100%",
        // transform: { translateX: "-100%"}
    },
    onscreen: {
        x: "0%",
        transition: {
            type: "spring",
            bounce: 0.4,
            duration: 0.8
        }
    }
};
export default function SkillBox({url, name, w = 50, h = 50}) {
    return (
        <motion.section
            initial={"offscreen"}
            whileInView="onscreen"
            viewport={{once: false, amount: 0.1}}>
            <motion.div variants={cardVariants}>
                <motion.div
                    className={"row border border-primary border-2 rounded-5 py-2 mb-3 mx-auto scale-up-center bg-primary-subtle"}
                    style={{maxWidth: "300px", boxShadow: "0px 0px 20px 5px black"}}
                    whileHover={{
                        boxShadow: "0px 0px 40px 20px black"
                    }}
                >
                    <img className={"col-auto ms-4"} src={url} width={w.toString()} height={h.toString()} alt={name}/>
                    <h3 className={"col-auto align-self-center"}> {name}</h3>
                </motion.div>
            </motion.div>
        </motion.section>
    )
}