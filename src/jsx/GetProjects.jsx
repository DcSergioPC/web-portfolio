const ListProjects = JSON.parse(import.meta.env.VITE_PROJECTS)
export default ListProjects
// Example of format expected to receive in JSON
/*
[
    {
        "title": {
            "en": "Manage coffee orders",
            "es": "Gestor de pedidos de café"
        },
        "description": {
            "en": "This project manages orders, customers, products and users for a coffee shop.",
            "es": "Este proyecto gestiona los pedidos, clientes, productos y usuarios de una cafetería."
        },
        "url": "https://dcsergiopc.gitlab.io/cafeteria-drf-django/",
        "repository": "https://gitlab.com/DcSergioPC/cafeteria-drf-django/",
        "technologies": [
            "mongo",
            "django",
            "react",
            "bootstrap"
        ],
        "info": [
            {
                "email": "administrador@mail.com",
                "password": 1234
            },
            {
                "email": "recepcionista@mail.com",
                "password": 1234
            },
            {
                "email": "cocinero@mail.com",
                "password": 1234
            }
        ]
    },
    {
        "title": {
            "en": "Rotate on Drag",
            "es": "Rotar al arrastrar"
        },
        "description": {
            "en": "This is a project to test some functions of Framer-Motion Framework",
            "es": "Este es un proyecto para probar algunas funciones de Framer-Motion Framework"
        },
        "url": "https://dcsergiopc.gitlab.io/react-framer-motion-rotate-on-drag/",
        "repository": "https://gitlab.com/DcSergioPC/react-framer-motion-rotate-on-drag/",
        "technologies": [
            "react"
        ],
        "info": false
    },
    {
        "title": {
            "en": "Tetris",
            "es": "Tetris"
        },
        "description": {
            "en": "Tetris game made entirely using javascript and canvas",
            "es": "Juego de Tetris hecho enteramente usando javascript y canvas"
        },
        "url": "https://dcsergiopc.gitlab.io/tetris-js-canvas/",
        "repository": "https://gitlab.com/DcSergioPC/tetris-js-canvas/",
        "technologies": [
            "javascript"
        ],
        "info": false
    }
]
*/