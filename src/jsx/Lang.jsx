import '../scss/select.css'
import {useTranslation} from 'react-i18next';

const Lang = () => {
    const {i18n} = useTranslation()
    const handleSelect = (e) => {
        i18n.changeLanguage(e.target.value)
        localStorage.setItem("lang", e.target.value)
    }
    return (
        <select
            style={{
                right: "20px",
                top: "20px",
                height: "min-content"
            }}
            title={"languages"}
            onChange={handleSelect}
            defaultValue={localStorage.getItem("lang") || "es"}
        >
            <option value="es">ES</option>
            <option value="en">EN</option>
        </select>
    );
};

export default Lang;