import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { resolve } from 'path';
// for FontAwesome

export default defineConfig({
  plugins: [react()],
  root: resolve(__dirname, 'src'),
  resolve: {
    alias: {
      '~bootstrap': resolve(__dirname, 'node_modules/bootstrap'),
    },
  },
  server: {
    // port: 8080,
    hot: true,
  },
  base: '/web-portfolio/'
});