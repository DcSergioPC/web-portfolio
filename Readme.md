# 🤖Hi, i'm Sergio, and this is the code of my web portfolio
## 🔗 Links
> [![My Skills](https://skillicons.dev/icons?i=linkedin)](https://www.linkedin.com/in/dcsergio/)
> [![My Skills](https://skillicons.dev/icons?i=github)](https://github.com/DcSergioPC)
## 🛠 Skills
![My Skills](https://skillicons.dev/icons?i=js,html,css,react,python,django,postgres,mongo,linux,docker)
## 🎨 Web Portfolio
> ## [dcsergiopc.gitlab.io/web-portfolio/](https://dcsergiopc.gitlab.io/web-portfolio/)
## 🧰 These technologies have been used to create this portfolio
> - [React 18.2.0](https://react.dev/)
> - [Framer Motion 10.12.18](https://www.framer.com/motion/)
> - [React Router 6.21.1](https://reactrouter.com/en/main)
> - [Bootstrap 5.3.0](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
> - [FontAwesome 6.4.0](https://fontawesome.com/)
> - [Vite 4.3.9](https://vitejs.dev/)
